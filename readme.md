# CorrientazoSystem

This repository was created to solve the technical challenge for the S4N interview.
In this will find the code associated to the "Su corrientazo a home" case in which
a restaurant manager needs to control a set of Drones for deliver each order to each customer in a range of
10 square around him / her restaurant.

## Developer

|Information|Details|
|-----------|-------|
|Name|Luis Felipe Mendivelso Osorio|
|Email|ing.felipe.mendivelso@gmail.com|
|Created at|2020-11-13|

## Stack
*   Java 8
*   Maven 3
*   JUnit 4