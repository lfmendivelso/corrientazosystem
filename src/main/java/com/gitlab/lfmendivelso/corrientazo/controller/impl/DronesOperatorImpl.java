package com.gitlab.lfmendivelso.corrientazo.controller.impl;

import com.gitlab.lfmendivelso.corrientazo.controller.DronesOperator;
import com.gitlab.lfmendivelso.corrientazo.helpers.PathCalculator;
import com.gitlab.lfmendivelso.corrientazo.models.Drone;
import com.gitlab.lfmendivelso.corrientazo.models.Position;
import com.gitlab.lfmendivelso.corrientazo.services.ReporterService;
import com.gitlab.lfmendivelso.corrientazo.services.SchedulerService;
import com.gitlab.lfmendivelso.corrientazo.services.impl.ReporterServiceImpl;
import com.gitlab.lfmendivelso.corrientazo.services.impl.SchedulerServiceImpl;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class DronesOperatorImpl implements DronesOperator {

    SchedulerService schedulerService;
    ReporterService reporterService;
    String inputFolder;
    String outputFolder;

    public DronesOperatorImpl(String inputFolder, String outputFolder) {
        this.inputFolder = inputFolder;
        this.outputFolder = outputFolder;
        this.schedulerService = new SchedulerServiceImpl();
        this.reporterService = new ReporterServiceImpl();
    }

    @Override
    public Map<String, Drone> startOperation() {
        Map<String, Drone> droneMap = new LinkedHashMap<>();
        List<Drone> drones = schedulerService.readFiles(inputFolder);
        for (Drone drone : drones) {
            Position current = drone.getCurrentPosition();
            for (String order : drone.getOrders().keySet()) {
                drone.setCurrentPosition(current);
                current = PathCalculator.calculateEndpoint(drone, order);
                drone.updatePositionByOrder(order, new Position(current.getX(), current.getY(), current.getCardinal()));
            }
            reporterService.generateReport(drone, outputFolder);
            droneMap.put(drone.getId(), drone);
        }
        return droneMap;
    }
}
