package com.gitlab.lfmendivelso.corrientazo.controller;

import com.gitlab.lfmendivelso.corrientazo.models.Drone;

import java.util.Map;

public interface DronesOperator {

    Map<String, Drone> startOperation();

}
