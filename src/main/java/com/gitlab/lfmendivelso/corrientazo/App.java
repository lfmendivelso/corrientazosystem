package com.gitlab.lfmendivelso.corrientazo;

import com.gitlab.lfmendivelso.corrientazo.controller.DronesOperator;
import com.gitlab.lfmendivelso.corrientazo.controller.impl.DronesOperatorImpl;
import com.gitlab.lfmendivelso.corrientazo.models.Drone;
import lombok.extern.java.Log;

import java.io.File;
import java.util.Map;

/**
 * Hello world!
 */
@Log
public class App {
    public static void main(String[] args) {

        log.info("---------------------------------");
        log.info("Bienvenido al Sistema Corrientazo");
        log.info("---------------------------------");

        String inputFolder = "data/inputs";
        String outputFolder = "data/outputs";
        inputFolder = getPath(inputFolder);
        outputFolder = getPath(outputFolder);

        log.info("------------------");
        log.info("Inicia la ejecución");
        log.info("Ruta de Entrada: " + inputFolder);
        log.info("Ruta de Destino: " + outputFolder);
        DronesOperator operator = new DronesOperatorImpl(inputFolder, outputFolder);
        Map<String, Drone> drones = operator.startOperation();
        log.info("------------------");
        log.info("Resultados:");
        for (Drone drone : drones.values()) {
            log.info("Dron N° " + drone.getId()
                    + " Posición final: (" + drone.getCurrentPosition().getX()
                    + "," + drone.getCurrentPosition().getY()
                    + "," + drone.getCurrentPosition().getCardinal().value()
                    + " Total de ordernes: " + drone.getOrders().size());
        }
    }

    public static String getPath(String path) {
        File file = new File(path);
        return file.getAbsolutePath();
    }
}
