package com.gitlab.lfmendivelso.corrientazo.services.impl;

import com.gitlab.lfmendivelso.corrientazo.models.Drone;
import com.gitlab.lfmendivelso.corrientazo.models.Position;
import com.gitlab.lfmendivelso.corrientazo.services.ReporterService;
import lombok.SneakyThrows;
import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;

@Log
public class ReporterServiceImpl implements ReporterService {

    @SneakyThrows
    @Override
    public void generateReport(Drone drone, String destinationPath) {
        if (StringUtils.isBlank(destinationPath)) {
            throw new IllegalArgumentException("The destination path is null/empty");
        }
        if (drone == null) {
            throw new IllegalArgumentException("The Drone object is null");
        }
        File outputFolder = new File(destinationPath);
        if (!outputFolder.exists()) {
            throw new IllegalArgumentException("The folder doesn't exist");
        }
        if (outputFolder.isFile()) {
            throw new IllegalArgumentException("The destination path is a file");
        }
        String path = destinationPath + "/out" + drone.getId() + ".txt";
        File previous = new File(path);
        if (previous.exists()) {
            previous.delete();
        }
        try (FileWriter output = new FileWriter(path)) {

            output.write("== Reporte de entregas ==\n");
            output.write("\n");
            for (Position position : drone.getOrders().values()) {
                output.write("(" + position.getX() + "," + position.getY() + ") dirección " + position.getCardinal().getWord() + "\n");
            }

        } catch (IOException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
    }

}
