package com.gitlab.lfmendivelso.corrientazo.services;

import com.gitlab.lfmendivelso.corrientazo.models.Drone;

public interface ReporterService {

    void generateReport(Drone drone, String destinationPath);

}
