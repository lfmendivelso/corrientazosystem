package com.gitlab.lfmendivelso.corrientazo.services.impl;

import com.gitlab.lfmendivelso.corrientazo.models.Drone;
import com.gitlab.lfmendivelso.corrientazo.services.SchedulerService;
import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Pattern;

@Log
public class SchedulerServiceImpl implements SchedulerService {

    @Override
    public List<Drone> readFiles(String path) {
        if (StringUtils.isBlank(path)) {
            throw new IllegalArgumentException("The destination path is null/empty");
        }
        File inputFolder = new File(path);
        if (!inputFolder.exists()) {
            throw new IllegalArgumentException("The folder doesn't exist");
        }
        if (inputFolder.isFile()) {
            throw new IllegalArgumentException("The destination path is a file");
        }
        List<Drone> drones = new ArrayList<>();
        File[] inputsFiles = inputFolder.listFiles((dir, name) -> name.endsWith(".txt"));
        try {
            for (File input : inputsFiles) {
                Drone drone = generateDrone(input);
                if (drone == null) {
                    throw new IllegalArgumentException("Error: Illegal Argument");
                }
                drones.add(drone);
            }
        } catch (IllegalArgumentException exception) {
            drones.clear();
            log.log(Level.SEVERE, exception.getMessage(), exception);
        }
        return drones;
    }

    private Drone generateDrone(File input) {
        Drone drone = null;
        try {
            List<String> orders = Files.readAllLines(Paths.get(input.getAbsolutePath()), StandardCharsets.UTF_8);
            if (orders.isEmpty()) {
                throw new IllegalArgumentException("The file " + input.getAbsolutePath() + " is empty");
            }
            if (isValidListOfOrder(orders)) {
                throw new IllegalArgumentException("The file " + input.getAbsolutePath() + " has a wrong input");
            }
            drone = new Drone();
            String id = input.getName().replaceAll("[in]|(\\.txt)$", "");
            drone.setId(id);
            for (String order : orders) {
                drone.addOrder(order);
            }
        } catch (IOException exception) {
            log.log(Level.SEVERE, exception.getMessage(), exception);
        }
        return drone;
    }

    private boolean isValidListOfOrder(List<String> orders) {
        Pattern pattern = Pattern.compile("[^AID]");
        return orders.stream().anyMatch(o -> pattern.matcher(o).find());
    }

}
