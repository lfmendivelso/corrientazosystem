package com.gitlab.lfmendivelso.corrientazo.services;

import com.gitlab.lfmendivelso.corrientazo.models.Drone;

import java.util.List;

public interface SchedulerService {

    List<Drone> readFiles(String path);

}
