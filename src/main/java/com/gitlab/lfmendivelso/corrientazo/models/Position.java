package com.gitlab.lfmendivelso.corrientazo.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Position {

    int x;
    int y;
    CardinalPoint cardinal;

    public Position() {
        this.x = 0;
        this.y = 0;
        this.cardinal = CardinalPoint.NORTH;
    }

    public Position(int x, int y, CardinalPoint cardinal) {
        this.x = x;
        this.y = y;
        this.cardinal = cardinal;
    }
}
