package com.gitlab.lfmendivelso.corrientazo.models;

public enum CardinalPoint {
    NORTH('N', "Norte"),
    SOUTH('S', "Sur"),
    WEST('W', "Occidente"),
    EAST('E', "Oriente");

    private final char letter;
    private final String word;

    CardinalPoint(char letter, String word) {
        this.letter = letter;
        this.word = word;
    }

    public char value() {
        return letter;
    }

    public String getWord() {
        return word;
    }
}
