package com.gitlab.lfmendivelso.corrientazo.models;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
public class Drone {

    String id;
    Position currentPosition;
    Map<String, Position> orders;

    public Drone() {
        id = null;
        currentPosition = new Position();
        orders = new LinkedHashMap<>();
    }

    public void addOrder(String order) {
        orders.put(order, new Position());
    }

    public void updatePositionByOrder(String order, Position position) {
        orders.put(order, position);
    }

}
