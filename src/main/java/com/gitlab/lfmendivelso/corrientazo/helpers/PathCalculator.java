package com.gitlab.lfmendivelso.corrientazo.helpers;

import com.gitlab.lfmendivelso.corrientazo.models.CardinalPoint;
import com.gitlab.lfmendivelso.corrientazo.models.Drone;
import com.gitlab.lfmendivelso.corrientazo.models.Position;

public class PathCalculator {

    private PathCalculator() {
        throw new IllegalStateException("This is a utility class");
    }

    public static Position calculateEndpoint(Drone drone, String order) {
        Position position = drone.getCurrentPosition();
        for (char move : order.toCharArray()) {
            switch (move) {
                case 'A':
                    go(position);
                    break;
                case 'I':
                    moveLeft(position);
                    break;
                case 'D':
                    moveRight(position);
                    break;
                default:
                    throw new IllegalArgumentException("Invalid move. Value: " + move);
            }
        }
        return position;
    }

    private static void moveLeft(Position position) {
        if (position.getCardinal() == CardinalPoint.NORTH) {
            position.setCardinal(CardinalPoint.WEST);
        } else if (position.getCardinal() == CardinalPoint.WEST) {
            position.setCardinal(CardinalPoint.SOUTH);
        } else if (position.getCardinal() == CardinalPoint.SOUTH) {
            position.setCardinal(CardinalPoint.EAST);
        } else {
            position.setCardinal(CardinalPoint.NORTH);
        }
    }

    private static void moveRight(Position position) {
        if (position.getCardinal() == CardinalPoint.NORTH) {
            position.setCardinal(CardinalPoint.EAST);
        } else if (position.getCardinal() == CardinalPoint.EAST) {
            position.setCardinal(CardinalPoint.SOUTH);
        } else if (position.getCardinal() == CardinalPoint.SOUTH) {
            position.setCardinal(CardinalPoint.WEST);
        } else {
            position.setCardinal(CardinalPoint.NORTH);
        }
    }

    private static void go(Position position) {
        if (position.getCardinal() == CardinalPoint.NORTH) {
            position.setY(position.getY() + 1);
        } else if (position.getCardinal() == CardinalPoint.EAST) {
            position.setX(position.getX() + 1);
        } else if (position.getCardinal() == CardinalPoint.SOUTH) {
            position.setY(position.getY() - 1);
        } else {
            position.setX(position.getX() - 1);
        }
        validateRadius(position);
    }

    private static void validateRadius(Position position) {
        if (position.getX() > 5 || position.getX() < -5 || position.getY() > 5 || position.getY() < -5) {
            throw new IllegalArgumentException("The order has a configuration which drive the drone out of the allowed limit");
        }
    }
}
