package com.gitlab.lfmendivelso.corrientazo.models;

import org.junit.Assert;
import org.junit.Test;

public class PositionTest {


    @Test
    public void testAccessors() {
        Position position = new Position();
        Assert.assertNotNull(position);
        Assert.assertEquals(0, position.getX());
        Assert.assertEquals(0, position.getY());
        Assert.assertEquals(CardinalPoint.NORTH, position.getCardinal());
        position.setCardinal(CardinalPoint.WEST);
        Assert.assertEquals(CardinalPoint.WEST, position.getCardinal());
        position.setX(1);
        position.setY(1);
        Assert.assertEquals(1, position.getX());
        Assert.assertEquals(1, position.getY());
    }

}
