package com.gitlab.lfmendivelso.corrientazo.models;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class DroneTest {

    @Test
    public void testAccessors() {
        Drone drone = new Drone();
        Assert.assertNotNull(drone);

        Assert.assertNull(drone.getId());
        String id = "01";
        drone.setId(id);
        Assert.assertEquals(id, drone.getId());

        Assert.assertNotNull(drone.getCurrentPosition());
        Position pos = new Position();
        pos.setCardinal(CardinalPoint.SOUTH);
        drone.setCurrentPosition(pos);
        Assert.assertEquals(pos.getCardinal(), drone.getCurrentPosition().getCardinal());

        Assert.assertNotNull(drone.getOrders());
        Assert.assertTrue(drone.getOrders().isEmpty());
        Map<String, Position> orders = new HashMap<>();
        orders.put("AIAAA", new Position());
        orders.put("ADAI", new Position());
        drone.setOrders(orders);
        Assert.assertEquals(2, drone.getOrders().size());
    }

    @Test
    public void testAddOrder() {
        Drone drone = new Drone();
        Assert.assertNotNull(drone.getOrders());
        Assert.assertTrue(drone.getOrders().isEmpty());
        String order = "AIDAA";
        drone.addOrder(order);
        Assert.assertFalse(drone.getOrders().isEmpty());
        Assert.assertEquals(1, drone.getOrders().size());
        Assert.assertTrue(drone.getOrders().containsKey(order));
    }

    @Test
    public void testUpdatePositionByOrder() {
        Drone drone = new Drone();
        Assert.assertNotNull(drone.getOrders());
        Assert.assertTrue(drone.getOrders().isEmpty());
        String order = "AAAIAA";
        drone.addOrder(order);
        Position pos = new Position();
        pos.setCardinal(CardinalPoint.EAST);
        drone.updatePositionByOrder(order, pos);
        Assert.assertEquals(CardinalPoint.EAST, drone.getOrders().get(order).getCardinal());
    }

}
