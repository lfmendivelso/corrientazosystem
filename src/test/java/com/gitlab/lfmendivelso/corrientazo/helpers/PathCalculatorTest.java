package com.gitlab.lfmendivelso.corrientazo.helpers;

import com.gitlab.lfmendivelso.corrientazo.models.CardinalPoint;
import com.gitlab.lfmendivelso.corrientazo.models.Drone;
import com.gitlab.lfmendivelso.corrientazo.models.Position;
import org.junit.Assert;
import org.junit.Test;


public class PathCalculatorTest {

    /**
     * Scenario 1: Only A
     * Input:
     * - Drone: (0,0,N)
     * - Order: AAAAA
     * Expected: Position (0,5,N)
     */
    @Test
    public void calculateEndpointOnlyA() {
        Drone drone = new Drone();
        String id = "01";
        String order = "AAAAA";
        drone.setId(id);
        drone.addOrder(order);
        Position expected = new Position(0, 5, CardinalPoint.NORTH);
        Position result = PathCalculator.calculateEndpoint(drone, order);
        Assert.assertEquals(expected.getX(), result.getX());
        Assert.assertEquals(expected.getY(), result.getY());
        Assert.assertEquals(expected.getCardinal(), result.getCardinal());
    }

    /**
     * Scenario 2: Only I
     * Input:
     * - Drone: (0,0,N)
     * - Order: IIIII
     * Expected: Position (0,0,W)
     */
    @Test
    public void calculateEndpointOnlyI() {
        Drone drone = new Drone();
        String id = "01";
        String order = "IIIII";
        drone.setId(id);
        drone.addOrder(order);
        Position expected = new Position(0, 0, CardinalPoint.WEST);
        Position result = PathCalculator.calculateEndpoint(drone, order);
        Assert.assertEquals(expected.getX(), result.getX());
        Assert.assertEquals(expected.getY(), result.getY());
        Assert.assertEquals(expected.getCardinal(), result.getCardinal());
    }

    /**
     * Scenario 3: Only D
     * Input:
     * - Drone: (0,0,N)
     * - Order: DDDDD
     * Expected: Position (0,0,E)
     */
    @Test
    public void calculateEndpointOnlyD() {
        Drone drone = new Drone();
        String id = "01";
        String order = "DDDDD";
        drone.setId(id);
        drone.addOrder(order);
        Position expected = new Position(0, 0, CardinalPoint.EAST);
        Position result = PathCalculator.calculateEndpoint(drone, order);
        Assert.assertEquals(expected.getX(), result.getX());
        Assert.assertEquals(expected.getY(), result.getY());
        Assert.assertEquals(expected.getCardinal(), result.getCardinal());
    }

    /**
     * Scenario 4: Mixed
     * Input:
     * - Drone: (0,0,N)
     * - Order: AAAAIAA
     * Expected: Position (-2,4,E)
     */
    @Test
    public void calculateEndpointMixed() {
        Drone drone = new Drone();
        String id = "01";
        String order = "AAAAIAA";
        drone.setId(id);
        drone.addOrder(order);
        Position expected = new Position(-2, 4, CardinalPoint.WEST);
        Position result = PathCalculator.calculateEndpoint(drone, order);
        Assert.assertEquals(expected.getX(), result.getX());
        Assert.assertEquals(expected.getY(), result.getY());
        Assert.assertEquals(expected.getCardinal(), result.getCardinal());
    }

    /**
     * Scenario 5: Mixed 2
     * Input:
     * - Drone: (1,1,N)
     * - Order: AAAD
     * Expected: Position (0,3,W)
     */
    @Test
    public void calculateEndpointMixed2() {
        Drone drone = new Drone();
        String id = "01";
        String order = "AAAD";
        drone.setId(id);
        drone.addOrder(order);
        Position expected = new Position(0, 3, CardinalPoint.EAST);
        Position result = PathCalculator.calculateEndpoint(drone, order);
        Assert.assertEquals(expected.getX(), result.getX());
        Assert.assertEquals(expected.getY(), result.getY());
        Assert.assertEquals(expected.getCardinal(), result.getCardinal());
    }

    /**
     * Scenario 6: Out of the limit 10 squares
     * Input:
     * - Drone: (4,0,N)
     * - Order: AAA
     * Expected: Should generate an exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void calculateEndpointOutLimits() {
        Drone drone = new Drone();
        String id = "01";
        String order = "AAA";
        drone.setId(id);
        drone.addOrder(order);
        drone.setCurrentPosition(new Position(0, 4, CardinalPoint.NORTH));
        PathCalculator.calculateEndpoint(drone, order);
    }

}
