package com.gitlab.lfmendivelso.corrientazo.controller;

import com.gitlab.lfmendivelso.corrientazo.controller.impl.DronesOperatorImpl;
import com.gitlab.lfmendivelso.corrientazo.models.Drone;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DronesOperatorTest {


    public static final String SCENARIO_PATH = "src/test/resources/dronesOperatorData";
    public static final String INPUT_PATH = SCENARIO_PATH + "/input";
    public static final String OUTPUT_PATH = SCENARIO_PATH + "/output";
    private DronesOperator operator;

    @Before
    public void setup() {
        operator = new DronesOperatorImpl(INPUT_PATH, OUTPUT_PATH);
    }

    @Test
    public void testStartOperation() {
        Map<String,Drone> droneMap = operator.startOperation();
        Assert.assertTrue(droneMap.containsKey("01"));
        Assert.assertTrue(droneMap.containsKey("02"));
        Assert.assertTrue(droneMap.containsKey("03"));
        File outputFolder = new File(getPath(OUTPUT_PATH));
        File[] results = outputFolder.listFiles((dir, name) -> name.endsWith(".txt"));
        Assert.assertEquals(3,results.length);
    }

    @After
    public void cleanUp() {
        File outputFolder = new File(getPath(OUTPUT_PATH));
        File[] results = outputFolder.listFiles((dir, name) -> name.endsWith(".txt"));
        for(File result : results){
            result.delete();
        }
    }

    public String getPath(String relativePath) {
        File file = new File(relativePath);
        return file.getAbsolutePath();
    }

}
