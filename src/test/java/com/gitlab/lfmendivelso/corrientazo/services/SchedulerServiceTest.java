package com.gitlab.lfmendivelso.corrientazo.services;

import com.gitlab.lfmendivelso.corrientazo.models.Drone;
import com.gitlab.lfmendivelso.corrientazo.services.impl.SchedulerServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class SchedulerServiceTest {

    public static final String SCENARIOS_PATH = "src/test/resources/schedulerData/scenario";
    private SchedulerService schedulerService;

    @Before
    public void setup() {
        schedulerService = new SchedulerServiceImpl();
    }

    public String getPath(int scenario) {
        String path = SCENARIOS_PATH + scenario;
        File file = new File(path);
        return file.getAbsolutePath();
    }

    /**
     * Scenario 1: Not files presents
     * Expected result: Should return an empty list of drones.
     */
    @Test
    public void tryToReadFilesWhenThereAreNoFiles() {
        String path = getPath(1);
        List<Drone> drones = schedulerService.readFiles(path);
        Assert.assertNotNull(drones);
        Assert.assertTrue(drones.isEmpty());
    }

    /**
     * Scenario 2: Only one file.
     * Expected result: Should return a list with a single item.
     */
    @Test
    public void readFilesWhenThereIsOnlyOneFile() {
        String path = getPath(2);
        Drone expected = new Drone();
        expected.setId("01");
        expected.addOrder("AAAAIAA");
        expected.addOrder("DDDAIAD");
        expected.addOrder("AAIADAD");
        List<Drone> drones = schedulerService.readFiles(path);
        Assert.assertNotNull(drones);
        Assert.assertFalse(drones.isEmpty());
        Assert.assertEquals(1, drones.size());
        Drone result = drones.get(0);
        Assert.assertEquals(expected.getId(), result.getId());
        Assert.assertTrue(result.getOrders().containsKey("AAAAIAA"));
        Assert.assertTrue(result.getOrders().containsKey("DDDAIAD"));
        Assert.assertTrue(result.getOrders().containsKey("AAIADAD"));
    }

    /**
     * Scenario 3: When there is only one file and this is empty
     * Expected result: Should return an empty list of drones.
     */
    @Test
    public void readFilesWhenThereIsOnlyOneFileAndThisIsEmpty() {
        String path = getPath(3);
        List<Drone> drones = schedulerService.readFiles(path);
        Assert.assertNotNull(drones);
        Assert.assertTrue(drones.isEmpty());
    }

    /**
     * Scenario 4: When there is only one file and this has a wrong input
     * Expected result: Should generate a exception.
     */
    @Test
    public void readFilesWhenThereIsOnlyOneFileAndThisHasAWrongInput() {
        String path = getPath(4);
        List<Drone> drones = schedulerService.readFiles(path);
        Assert.assertTrue(drones.isEmpty());
    }

    /**
     * Scenario 5: When there are multiple files
     * Expected result: Should return a list of the same size of the number of files.
     */
    @Test
    public void readFilesWhenThereAreMultipleFiles() {
        String path = getPath(5);
        Drone expected1 = new Drone();
        expected1.setId("01");
        Drone expected2 = new Drone();
        expected2.setId("02");
        Drone expected3 = new Drone();
        expected3.setId("03");
        List<Drone> drones = schedulerService.readFiles(path);
        Assert.assertNotNull(drones);
        Assert.assertFalse(drones.isEmpty());
        Assert.assertEquals(3, drones.size());
        Assert.assertTrue(drones.stream().anyMatch(d -> d.getId().equals(expected1.getId())));
        Assert.assertTrue(drones.stream().anyMatch(d -> d.getId().equals(expected2.getId())));
        Assert.assertTrue(drones.stream().anyMatch(d -> d.getId().equals(expected3.getId())));
    }

    /**
     * Scenario 6: When there are multiple files and one file of them has a wrong input.
     * Expected result: Should generate an exception.
     */
    @Test
    public void readFilesWhenThereAreMultipleFilesAndOneFileHasAWrongInput() {
        String path = getPath(6);
        Drone expected1 = new Drone();
        expected1.setId("01");
        List<Drone> drones = schedulerService.readFiles(path);
        Assert.assertTrue(drones.isEmpty());
    }

    /**
     * Scenario 7: When the folder doesn't exist.
     * Expected result: Should generate an exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void tryToReadFilesWhenFolderDoesNotExist() {
        String path = getPath(7);
        schedulerService.readFiles(path);
    }

    /**
     * Scenario 8: When the target path is a file.
     * Expected result: Should generate an exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void tryToReadFilesWhenThePathIsAFile() {
        String path = getPath(8);
        schedulerService.readFiles(path);
    }
}
