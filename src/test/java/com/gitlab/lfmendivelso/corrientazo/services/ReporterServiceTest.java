package com.gitlab.lfmendivelso.corrientazo.services;

import com.gitlab.lfmendivelso.corrientazo.models.Drone;
import com.gitlab.lfmendivelso.corrientazo.services.impl.ReporterServiceImpl;
import lombok.extern.java.Log;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Log
public class ReporterServiceTest {

    private ReporterService reporterService;

    @Before
    public void setup() {
        reporterService = new ReporterServiceImpl();
    }

    public String getTemporalPath() {
        String path = "data/outputs";
        File file = new File(path);
        return file.getAbsolutePath();
    }

    /**
     * Scenario 0: When the destination path is empty or null
     * Expected result: Should not generate any report.
     */
    @Test(expected = IllegalArgumentException.class)
    public void tryToGenerateReportWhenThePathIsNull() {
        reporterService.generateReport(new Drone(), null);
    }

    /**
     * Scenario 1: When the Drone object is null
     * Expected result: Should not generate any report.
     */
    @Test(expected = IllegalArgumentException.class)
    public void tryToGenerateReportWhenTheDroneIsNull() {
        String path = getTemporalPath();
        reporterService.generateReport(null, path);
    }

    /**
     * Scenario 2: When the destination Path is a file
     * Expected result: Should not generate any report, and generate exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void tryToGenerateReportWhenTheDestinationPathIsAFile() {
        String path = getTemporalPath() + "/.keep";
        Drone drone = new Drone();
        reporterService.generateReport(drone, path);
    }

    /**
     * Scenario 3: When the destination Path targets folders which doesn't exist.
     * Expected result: Should not generate any report, and generate exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void tryToGenerateReportWhenTheDestinationPathDoesNotExist() {
        String path = getTemporalPath() + "2";
        Drone drone = new Drone();
        reporterService.generateReport(drone, path);
    }

    /**
     * Scenario 4: Normal Scenario
     * Expected result: Should generate a report for the Drone.
     */
    @Test
    public void generateReport() {
        String path = getTemporalPath();
        Drone drone = new Drone();
        drone.setId("01");
        String order = "AAA";
        drone.addOrder(order);
        reporterService.generateReport(drone, path);
        List<String> list;
        Path outputPath = Paths.get(path + "/out01.txt");
        File output = outputPath.toFile();
        try {
            if (!output.exists()) {
                Assert.fail("The output file was not created.");
            }
            list = Files.readAllLines(outputPath, StandardCharsets.UTF_8);
            Assert.assertEquals("== Reporte de entregas ==", list.get(0));
            Assert.assertEquals("", list.get(1));
            Assert.assertEquals("(0,0) dirección Norte", list.get(2));

        } catch (IOException e) {
            Assert.fail(e.getMessage());
        } finally {
            if (output.exists()) {
                output.delete();
            }
        }
    }


}
